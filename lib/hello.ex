defmodule Hello do
  import Supervisor.Spec

  def hello do
    IO.puts "hello world!"
  end

  def load_nifs do
    :ok = :fxml_stream.load_nif()
    :ok = :fxml.load_nif()
  end
  
  def start(_a, _b) do
    load_nifs
    children = [ worker(Hello.Robot, []) ]
    Supervisor.start_link(children, strategy: :one_for_one)
  end
end
