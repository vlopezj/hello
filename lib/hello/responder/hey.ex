defmodule Hello.Responders.Hey do
  @moduledoc """
  Dumb responses to dumb questions
  """

  use Hedwig.Responder

  @usage """
  hola - Saluda
  """
  hear ~r/^hola/i, msg do
    reply msg, "carambola"
  end

  @usage """
  di <query> - Repite algo (eco)
  """
  hear ~r/^di (?<query>.*)/, msg do
    reply msg, "¡" <> msg.matches["query"] <> "!"
  end

end
