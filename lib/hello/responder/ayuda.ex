defmodule Hello.Responders.Ayuda do
  @moduledoc """
  Ayuda.
  """

  use Hedwig.Responder

  @usage """
  ayuda - Muestra los comandos que hobbes conoce
  """
  hear ~r/ayuda/, msg, state do
    send msg, display_usage(state)
  end

  @usage """
  ayuda <patrón> - Muestra los commandos que corresponden con <query>.
  """
  hear ~r/ayuda (?<query>.*)/, msg, state do
    send msg, search(state, msg.matches["query"])
  end

  defp display_usage(state) do
    state
    |> all_usage()
    |> Enum.reverse()
    |> Enum.map_join("\n", &(&1))
  end

  defp search(state, query) do
    state
    |> all_usage()
    |> Enum.reverse()
    |> Enum.filter(&(String.match?(&1, ~r/(#{query})/i)))
    |> Enum.map_join("\n", &(&1))
  end

  defp all_usage(%{name: name, robot: robot}) do
    responders = Hedwig.Robot.responders(robot)
    Enum.reduce responders, [], fn {mod, _opts}, acc ->
      mod.usage(name) ++ acc
    end
  end
end
