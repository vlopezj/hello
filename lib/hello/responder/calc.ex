defmodule Hello.Responders.Calc do
  @moduledoc """
  Use units to compute expressions
  """

  use Hedwig.Responder

  @usage """
  calcula <expr> - Calcula cosas
  """
  hear ~r/calc(ula)? (?<expr>.*)/, msg do
    expr = msg.matches["expr"]
    {result, code} = System.cmd("units",["-t",expr])
    send msg, expr <> " = " <> String.strip(result)
  end

  @usage """
  convierte <expr> a <unidad> - Convierte cosas
  """
  hear ~r/conv(ierte)? (?<expr>.*) (a|to) (?<unidad>.*)/, msg do
    expr = msg.matches["expr"]
    unidad = msg.matches["unidad"]
    {result, code} = System.cmd("units",["-t",expr,unidad])
    send msg, expr <> " son " <> String.strip(result) <> " " <> unidad
  end

end

