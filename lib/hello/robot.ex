defmodule Hello.Robot do
  use Hedwig.Robot, otp_app: :hello, adapter: Hedwig.Adapters.XMPP
end
