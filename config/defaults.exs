# This file is responsible for configuring your application
# and its dependencies with the aid of the Mix.Config module.
use Mix.Config

config :hello, Hello.Robot,
  responders: [
    {Hello.Responders.Hey, []},
    {Hello.Responders.Ayuda, []},
    {Hello.Responders.Calc, []},
  ]
