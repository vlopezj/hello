How to use:

- Create a config file in `config/config.exs`. You can base it off
  `config/config.exs.sample`.

- Run `mix run --no-halt`. Alternatively, use `iex -S mix`.
